package pdm.a05.todolist;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Un elemento della TodoList
 * @author Mauro Ferrari
 *
 */
public class TodoItem {

	private String todo;  // il task
	private GregorianCalendar createOn; // la data di creazione del task

	public TodoItem(String todo) {
		super();
		this.todo = todo;
		this.createOn = new GregorianCalendar();
	}

	@Override
  public String toString() {
		String currentDate = new SimpleDateFormat("dd/MM/yyyy").format(createOn.getTime());
		return currentDate + ": " + todo;
  }

	
	
}
