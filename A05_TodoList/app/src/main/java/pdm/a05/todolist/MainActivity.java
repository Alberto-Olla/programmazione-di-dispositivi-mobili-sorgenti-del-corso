package pdm.a05.todolist;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// get the references to the views
		final ListView listView = (ListView) findViewById(R.id.list_view);
		final EditText editText = (EditText) findViewById(R.id.input_insert_todo);
		final Button addButton = (Button) findViewById(R.id.button_add);

		// the array list containing the todo items
		final ArrayList<TodoItem> todoItems = new ArrayList<TodoItem>();

		// the adapter populating the listview
		final ArrayAdapter<TodoItem> adapter = new ArrayAdapter<TodoItem>(this,
		    android.R.layout.simple_list_item_1, todoItems);

		listView.setAdapter(adapter);

		addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TodoItem newTodo = new TodoItem(editText.getText().toString());
                todoItems.add(0, newTodo);
                adapter.notifyDataSetChanged();
                // clear text field
                editText.setText("");
                // hide the softkey
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        MainActivity.this.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
                // EditText give up focus
                EditText editText = (EditText) findViewById(R.id.input_insert_todo);
                editText.clearFocus();
            }
        });


        // set the onItemClick handler for the listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Click on item: (position=" + position + ", id = " + id + ")",
                        Toast.LENGTH_LONG).show();
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Long-click on item: (position=" + position + ", id = " + id + ")",
                        Toast.LENGTH_LONG).show();
                return true;
            }
        });

	}
}