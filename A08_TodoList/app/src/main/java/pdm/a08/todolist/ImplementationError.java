package pdm.a08.todolist;

class ImplementationError extends RuntimeException {

    public ImplementationError() {
        super("Implementation error!!");
    }

    public ImplementationError(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ImplementationError(String detailMessage) {
        super(detailMessage);
    }

    public ImplementationError(Throwable throwable) {
        super(throwable);
    }

}
