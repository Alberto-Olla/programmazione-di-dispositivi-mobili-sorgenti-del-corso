package pdm.a08.todolist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";
    private static final int SUBACTIVITY_NEW_TODO_ITEM = 1;
    private DBAdapter dbAdapter;
    private ArrayList<TodoItem> todoItems;  // the list containing the items
    private CustomArrayAdapter listViewAdapter; // the adapter populating the listview

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content layout
        setContentView(R.layout.activity_main);
        // set the action bar
        Toolbar tb = (Toolbar) findViewById(R.id.main_activity_toolbar);
        this.setActionBar(tb);


        // open db connection
        dbAdapter = DBAdapter.getInstance(this);

        // get the references to the views
        final ListView listView = (ListView) findViewById(R.id.list_view);

        // the list containing the todoitems
        todoItems = new ArrayList<TodoItem>();

        // the adapter populating the listview
        listViewAdapter = new CustomArrayAdapter(this, todoItems);

        // set the adapter
        listView.setAdapter(listViewAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
        // open the DB and update the listview
        dbAdapter.open();
        fillData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause(): closing DB");
        // onPause should close the database so that it frees up resources
        // for other Activities. This is also called when the Activity is
        // destroyed or finished so there’s no need to close the database
        // “onFinish” or “onDestroy”.
        dbAdapter.close();
    }


    private void fillData() {
        Log.v(TAG, "fillData() - Retriving all data to fill listview.");
        Cursor cursor = dbAdapter.getAllEntries();
        this.todoItems.clear();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            // Retrive todoitem data from cursor
            long idx = cursor.getLong(cursor.getColumnIndex(DBContract.TodoItems._ID));
            String task = cursor.getString(cursor.getColumnIndex(DBContract.TodoItems.COLUMN_NAME_TASK));
            GregorianCalendar creationDate = new GregorianCalendar();
            creationDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(DBContract.TodoItems.COLUMN_NAME_CREATION_DATE)));
            boolean isDueDateDefined = cursor.getInt(cursor.getColumnIndex(DBContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED)) == 1;
            GregorianCalendar dueDate = null;
            if (isDueDateDefined) {
                dueDate = new GregorianCalendar();
                dueDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(DBContract.TodoItems.COLUMN_NAME_DUE_DATE)));
            }

            // get the image
            Bitmap image = null;
            byte[] bytes = cursor.getBlob(cursor.getColumnIndex(DBContract.TodoItems
                    .COLUMN_NAME_IMAGE));
            if (bytes != null) {
                try {
                    image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                } catch (Exception e) {
                    Log.e(TAG, "Exception", e);
                }
            }


            // build the TodoItem instance
            TodoItem newItem = new TodoItem(this, idx, task, creationDate, isDueDateDefined,
                    dueDate,image);
            this.todoItems.add(0, newItem);
            cursor.moveToNext();
        }
        cursor.close();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    /*
     * Menu actions
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_item: { // start sub-activity for result
                Intent intent = new Intent(this.getApplicationContext(), NewTodoItemActivity.class); // Explicit intent creation
                startActivityForResult(intent, SUBACTIVITY_NEW_TODO_ITEM); // Start as sub-activity for result
                return true;
            }
            case R.id.action_delete_DB: {
                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.dialog_cleardb_database_confirm);
                // Add the buttons
                builder.setPositiveButton(R.string.dialog_cleardb_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dbAdapter.deleteTodoItemsTable();
                        todoItems.clear();
                        listViewAdapter.notifyDataSetChanged();
                    }
                });

                builder.setNegativeButton(R.string.dialog_cleardb_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
            case R.id.action_settings:
                showNonImplemented();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SUBACTIVITY_NEW_TODO_ITEM:
                switch (resultCode) {
                    case Activity.RESULT_OK: // add the new todoItem
                        TodoItem item = TodoItem.buildFrom(this, data.getExtras());
                        todoItems.add(0, item);
                        listViewAdapter.notifyDataSetChanged();
                        dbAdapter.open();
                        dbAdapter.insert(item);
                        return;
                    case Activity.RESULT_CANCELED: // nothing to do
                        return;
                    default:
                        throw new ImplementationError();
                }
            default:
                throw new ImplementationError();
        }
    }


    private void showNonImplemented() {
        String msg = "Not implemented yet!";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

}