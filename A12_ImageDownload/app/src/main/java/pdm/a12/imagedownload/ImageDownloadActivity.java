package pdm.a12.imagedownload;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;
import android.widget.ViewSwitcher;

public class ImageDownloadActivity extends Activity {

	private static String TAG = "ImageDownloadActivity";

	// Image URL
    private static final String URL_IMG = "http://imgsrc.hubblesite.org/hu/db/images/hs-2006-10-a-2560x1024_wallpaper.jpg";

	// Progress Bar
	private ViewSwitcher viewSwitcher;
	private ProgressBar progressBar;
	private ImageView imageView;
	private Button buttonDownload;
	private ImageDownloader imageDownloader;

	// Dialog ID
	public static final int PROGRESSBAR_DIALOG = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // set content layout
        setContentView(R.layout.main);
        // set the action bar
        Toolbar tb = (Toolbar) findViewById(R.id.main_activity_toolbar);
        this.setActionBar(tb);

		// get the viewSwitcher
		viewSwitcher = (ViewSwitcher) findViewById(R.id.viewswitcher);

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		progressBar.setProgress(0);

		imageView = (ImageView) findViewById(R.id.img_view);

		// Download button
		buttonDownload = (Button) findViewById(R.id.download_button);
		buttonDownload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				viewSwitcher.showNext();
				imageDownloader = new ImageDownloader();
				imageDownloader.execute(URL_IMG);
			}
		});
	}

	/**
	 * Background AsyncTask 
	 */
	class ImageDownloader extends AsyncTask<String, Integer, Bitmap> {

        // file for saving image in internal storage
        private final File OUTPUT_FILE = new File(getFilesDir(),"downloadedfile.jpg");

		private int progressCount; // progress
		private Bitmap downloadedImage = null;

		// executed on the UI-thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			buttonDownload.setEnabled(false);
			progressCount = 0;
			progressBar.setProgress(progressCount);
            progressBar.setMax(100);
		}

		// background worker
		@Override
		protected Bitmap doInBackground(String... imageURL) {

			int count;
			try {
				URL url = new URL(imageURL[0]);
				URLConnection conection = url.openConnection();
				conection.connect();

				// get the length of the file
				int lenghtOfFile = conection.getContentLength();

				// download the file using an 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream
				OutputStream output = new FileOutputStream(OUTPUT_FILE);

				byte data[] = new byte[1024];

				// byte counter
				long total = 0;
				while ((count = input.read(data)) != -1 && !isCancelled()) {
					total += count;
					// publish the progress 
					publishProgress((int) ((total * 100) / lenghtOfFile));
					output.write(data, 0, count);
				}
				output.flush(); // flush 
				output.close();
				input.close();
				InputStream fis = new FileInputStream(OUTPUT_FILE);
				downloadedImage = BitmapFactory.decodeStream(fis);
				return downloadedImage;
			} catch (Exception e) {
				Log.e("Error: ", e.getMessage());
				return null;
			}

		}

		// executed on the UI-thread
		protected void onProgressUpdate(Integer... progress) {
			// update the progress bar
			progressBar.setProgress(progress[0]);
		}

		// executed on the UI-thread
		@Override
		protected void onPostExecute(Bitmap image) {
			viewSwitcher.showNext();
			if (image != null)
				imageView.setImageBitmap(image);
			buttonDownload.setEnabled(true);
		}

		@Override
		protected void onCancelled() {
			Log.i(TAG, "Download cancelled.");
			Toast.makeText(ImageDownloadActivity.this, "Download Cancelled", Toast.LENGTH_LONG).show();
			viewSwitcher.showNext();
			buttonDownload.setEnabled(true);
		}

	}
}