#Sorgenti per il corso di "Programmazione di dispositivi mobili", edizione 2015/16


##Struttura


Il repository contiene il codice sorgente dei progetti presentati nel
corso. Ogni progetto corrisponde ad un'applicazione.

Il nome delle applicazioni è così composto

> AXX_nome 

dove XX è un numero progressivo che indica l'ordine di presentazione
dell'applicazione nel corso. Ogni progetto è organizzato in uno o più
package il cui nome è così strutturato

> pdm.aXX.<nome_package> 



##Importazione in Android Studio


Importare il progetto utilizzando il comando:

> File > New > Import Project 



##Descrizione dei progetti



####A01_HelloWorld

  Applicazione generata con il wizard di AndroidStudio con il solo
  scopo di testare che tutto funzioni correttamente.

####A02_ButtonExample

  Una semplice applicazione che gestisce 3 bottoni mediante
  registrazione dei listener direttatmente nei Button.

#### A03_ButtonExample
  Una semplice applicazione che gestisce dei Button mediante
  implementazione dell'interfaccia del listener nell'activity.

#### A04_ButtonExample
  Una semplice applicazione che gestisce dei Button mediante
  definizione degli event handler nell'elemento <Button> del layout XML.

#### A05_TodoList
  Applicazione TodoList, interfaccia con ListView (senza salvataggio
  dei TodoItem).

#### A06_TodoList
  Applicazione TodoList con database SQLite.


#### A07_TodoList
  Applicazione TodoList con toolbar e option menu, subatctivity per la
  creazione di un nuovo TodoItem.

#### A08_TodoList
  Utilizzo di intent impliciti per utilizzare la
  fotocamere. L'imamgine viene memorizzata come blob nel DB.  Utilizzo
  di una view definita esplicitamente per la visulizzazione degli
  elementi della ListView e definizione del relativo Adapter.

#### A09_TodoList
  L'immagine viene salvata nella memoria interna, nel DB viene
  memorizzato l'URI dell'immagine.

#### A10_ImageDownload
  Soluzione con utilizzo di Activity.runOnUiThread() e View.post().

#### A11_ImageDownload
  Soluzione con AsyncTask (senza utilizzo di Progress).

#### A12_ImageDownload
  Soluzione con AsyncTask e Progress Bar.

#### A13_TodoList
   Implementazione del content provider per la todolist. Utilizzo di
   CursorLoader per il caricamento del dati nella listview principale.
   

#### A14_UsingTodoListContentProvider
   Applicazione che utilizza il content provider implementato in A13_TodoList.
   Consente di visulizzare la todolist e di inserire nuovi elementi.

#### A15_TodoList
   Servizio di notifica dei prossimi todo e applicazione per la gestione del servizio.
   BroadcastReveicer per l'attivazione del servizio al boot del dispositivo.

#### A16_PermissionExample
   Esempio di utilizzo delle runtime permission per l'accesso alla lista dei contatti.

#### A17_Location
   Esempio di utilizzo delle funzionalità base dei servizi di localizzazione.

#### A18_AndroidLocation
  Esempio di utilizzo delle funzionalità base dei servizi di localizzazione con
  definizione dei criteri di selezione del provider e utilizzo di ProximityAlert.

#### A19_FragmentExample
  Esempio di utilizzo dei fragment e di gestione di layout differenti per handset e tablet.


#### A20_MapsExample
  Esempio di utilizzo di Google Maps. Per poter eseguire l'applicazione è necessario
  specificare il valore della API Key nel file
  A20_MapsExample/app/src/debug/res/values/google_maps_api.xml



  



