package pdm.a15.todolist.service;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;

import pdm.a15.todolist.R;
import pdm.a15.todolist.TodoItem;
import pdm.a15.todolist.contentprovider.TodoListContentProviderContract;

public class TodoListService extends Service {

    private final String TAG = "TodoListService";

    private static int SERVICE_RUNNING_NOTIFICATION_ID = 1;
    private static int FORTHCOMING_EVENT_INOTIFICATION_ID = 2;
    private static int NOTIFICATION_TODO_ICON = R.drawable.ic_event_busy; // service icon
    private static int NOTIFICATION_SERVICE_ICON = R.drawable.ic_event_note;
    private static long REFRESH_PERIOD = 1; //refresh time in minutes

    private ScheduledExecutorService scheduledExecutorService;

    private ArrayList<TodoItem> forthcomingEvents; // forthcoming events
    private ArrayList<TodoItem> alreadyNotified; // notified events

    @Override
    public void onCreate() {
        super.onCreate();
        alreadyNotified = new ArrayList<TodoItem>();
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {

            public void run() {
                refreshForthcomingEvents();
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify
                        (SERVICE_RUNNING_NOTIFICATION_ID, buildServiceRunningNotification());
            }
        }, 0, REFRESH_PERIOD, TimeUnit.MINUTES);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        refreshForthcomingEvents();
        // make this service run in the foreground supplying the ongoing
        // notification to be shown to the user while in this state
        startForeground(SERVICE_RUNNING_NOTIFICATION_ID, buildServiceRunningNotification());
        return Service.START_STICKY;
    }

    // builds the service running notification
    private Notification buildServiceRunningNotification() {
        // define notification details
        CharSequence tickerText = "TodoListService"; // ticker-text
        long when = System.currentTimeMillis(); // notification time
        CharSequence contentTitle = "TodoListService Running"; // message title
        CharSequence contentText = "Last refresh: " + DateFormat.format("h:mmaa", when) + ", "
                + forthcomingEvents.size() + " forthcoming events";
        // notification builder
        Notification.Builder nbuilder = new Notification.Builder(this);
        nbuilder.setSmallIcon(NOTIFICATION_SERVICE_ICON).
                setTicker(tickerText).setWhen(when).setContentTitle(contentTitle).
                setContentText(contentText);
        // build the intent to launche activity to manage the service
        Intent launchintent = new Intent(this, TodoListServiceActivity.class);
        launchintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // set the pending intent
        nbuilder.setContentIntent(PendingIntent.getActivity(this, 0, launchintent, 0));
        return nbuilder.build();
    }


    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return sBinder;
    }

    // IBinder definition
    private final IBinder sBinder = new SimpleBinder();

    class SimpleBinder extends Binder {

        TodoListService getService() {
            return TodoListService.this;
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        scheduledExecutorService.shutdown();
        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancelAll();
        forthcomingEvents.clear();
        super.onDestroy();
    }

    // public method invoked to refresh forthcoming events
    public void update() {
        new Runnable() {

            @Override
            public void run() {
                refreshForthcomingEvents();
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify
                        (SERVICE_RUNNING_NOTIFICATION_ID, buildServiceRunningNotification());
            }
        }.run();
    }

    // public method to get the number of forthcoming events
    public int getNumberOfForthcomingEvents() {
        return forthcomingEvents.size();
    }

    // refresh the list of forthcoming events and notify the new elements
    private void refreshForthcomingEvents() {
        // build the query: todoitems with due date after now
        long now = new GregorianCalendar().getTimeInMillis();
        String select = TodoListContentProviderContract.TodoItems.COLUMN_NAME_DUE_DATE + ">?";
        String[] selectionArgs = new String[]{String.valueOf(now)};
        String sortBy = TodoListContentProviderContract.TodoItems.COLUMN_NAME_DUE_DATE + " ASC";

        // execute the quesry in the content provider
        Cursor cursor = getContentResolver().query(TodoListContentProviderContract.CONTENT_URI, null,
                select, selectionArgs, sortBy);

        // define the list of forthcoming events
        this.forthcomingEvents = new ArrayList<TodoItem>();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            TodoItem item = TodoItem.buildFromContentProviderCursor(getBaseContext().getApplicationContext(), cursor);
            forthcomingEvents.add(0, item);
            cursor.moveToNext();
        }
        cursor.close();

        Log.i(TAG,
                " forthcoming events = " + forthcomingEvents == null ? "-1" : "" +
                        forthcomingEvents.size());

        // notify the new events
        for (TodoItem item : forthcomingEvents)
            if (!alreadyNotified.contains(item)) {
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE))
                        .notify(String.valueOf(item.getID()), FORTHCOMING_EVENT_INOTIFICATION_ID,
                                buildForthcomingEventNotification(item));
                alreadyNotified.add(item);
            }
    }


    // Build a fothcoming event notification
    private Notification buildForthcomingEventNotification(TodoItem item) {
        // build notification
        Notification.Builder nbuilder = new Notification.Builder(this);
        nbuilder.setSmallIcon(NOTIFICATION_TODO_ICON);
        nbuilder.setTicker("Fothcoming event!");
        nbuilder.setWhen(System.currentTimeMillis());
        nbuilder.setContentTitle("Forthcoming event");
        nbuilder.setContentText(item.getTask());

        nbuilder.setAutoCancel(true); // cancel notification if selected
        nbuilder.setDefaults(Notification.DEFAULT_ALL);

        Intent launchintent = new Intent(this, pdm.a15.todolist.MainActivity.class);
        launchintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pending = PendingIntent.getActivity(this, 0, launchintent, 0);
        nbuilder.setContentIntent(pending);
        return nbuilder.build();
    }

}