package pdm.a15.todolist.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import pdm.a15.todolist.service.TodoListService;

public class TodoListBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent serviceIntent = new Intent(context, TodoListService.class);
		context.startService(serviceIntent);
	}
}
