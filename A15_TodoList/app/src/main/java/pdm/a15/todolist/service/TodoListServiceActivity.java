package pdm.a15.todolist.service;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import pdm.a15.todolist.R;

public class TodoListServiceActivity extends Activity {

    private static final String SERVICE_NAME = pdm.a15.todolist.service.TodoListService.class
            .getCanonicalName();
    private Intent serviceIntent;

    // UI Buttons and texts
    private Button startButton;
    private Button stopButton;
    private Button checkButton;
    private Button updateButton;
    private TextView tvStatus;
    private TextView tvServiceDetails;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todolist_service);
        serviceIntent = new Intent(this.getApplicationContext(), pdm.a15.todolist.service.TodoListService.class);

        tvStatus = (TextView) findViewById(R.id.service_status);
        tvStatus.setText(isServiceRunning() ? getString(R.string.service_is_running)
                : getString(R.string.service_not_running));
        tvServiceDetails = (TextView) findViewById(R.id.service_number_of_events);
        tvServiceDetails.setText("Details unknown");

        startButton = (Button) findViewById(R.id.service_start_button);
        stopButton = (Button) findViewById(R.id.service_stop_button);
        checkButton = (Button) findViewById(R.id.service_check_button);
        updateButton = (Button) findViewById(R.id.service_update_button);
    }

    // onClick for Start Service Button
    public void onStartService(View v) {
        if (isServiceRunning())
            Toast.makeText(TodoListServiceActivity.this, "Service is already running!!",
                    Toast.LENGTH_SHORT).show();
        else {
            startService(serviceIntent);
            Intent intent = new Intent(TodoListServiceActivity.this, TodoListService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            checkButton.setEnabled(true);
            updateButton.setEnabled(true);
            tvStatus.setText(getString(R.string.service_is_running));
        }
    }

    // onClick for Stop Service Button
    public void onStopService(View v) {
        if (!isServiceRunning())
            Toast.makeText(TodoListServiceActivity.this, "Service is already stopped!!",
                    Toast.LENGTH_SHORT).show();
        else {
            // Unbind from the service
            unbindService(mConnection);
            updateButton.setEnabled(false);
            checkButton.setEnabled(false);
            tvServiceDetails.setText("Details unkown");
            stopService(serviceIntent);
            tvStatus.setText(getString(R.string.service_not_running));
        }
    }

    // onClick for Check Service Button
    public void onCheckService(View v) {
        tvServiceDetails.setText("Number of forthcoming events = "
                + lservice.getNumberOfForthcomingEvents());
    }

    // onClick for Update Service Button
    public void onUpdate(View v) {
        lservice.update();
    }


    @Override
    protected void onResume() {
        if (isServiceRunning()) { // if the service is running bind the service
            Intent intent = new Intent(TodoListServiceActivity.this, TodoListService.class);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        } else { // disable button that use the connection
            updateButton.setEnabled(false);
            checkButton.setEnabled(false);
        }
        super.onResume();
    }

    // Check if service is running
    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SERVICE_NAME.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    TodoListService lservice;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            lservice = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TodoListService.SimpleBinder sBinder = (TodoListService.SimpleBinder) service;
            lservice = sBinder.getService();
        }
    };

    @Override
    protected void onPause() {
        // Unbind from the service
        if (isServiceRunning())
            unbindService(mConnection);
        super.onPause();
    }


}