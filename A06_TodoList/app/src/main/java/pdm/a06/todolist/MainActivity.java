package pdm.a06.todolist;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private DBAdapter dbAdapter;

    private ArrayList<TodoItem> todoItems;  // the list containing the items
    private ArrayAdapter<TodoItem> listViewAdapter; // the adapter populating the listview

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content layout
        setContentView(R.layout.activity_main);

        // open db connection
        dbAdapter = DBAdapter.getInstance(this);

        // get the references to the views
        final ListView listView = (ListView) findViewById(R.id.list_view);

        // the list containing the todoitems
        todoItems = new ArrayList<TodoItem>();

        // the adapter populating the listview
        listViewAdapter = new ArrayAdapter<TodoItem>(this,
                android.R.layout.simple_list_item_1, todoItems);

        // set the adapter
        listView.setAdapter(listViewAdapter);

        // set the onItemClick handler for the listview
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),
                        "Click on item: (position=" + position + ", id = " + id + ")",
                        Toast.LENGTH_LONG).show();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long itemId) {
                TodoItem item = (TodoItem) listView.getItemAtPosition(position);

                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.dialog_delete_confirm_deletion);


                // Add the buttons
                builder.setPositiveButton(R.string.dialog_delete_confirm_deletion, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // get the item delected for delation
                        TodoItem item = (TodoItem) listView.getItemAtPosition(position);
                        Toast.makeText(getApplicationContext(),
                                "Item " + position + " " + item.toString(),
                                Toast.LENGTH_LONG).show();
                        // delete the item from the DB
                        dbAdapter.delete(item);
                        // delete the item from the listview data structure
                        todoItems.remove(position);
                        // notify the data set has changed to the listview adapter
                        listViewAdapter.notifyDataSetChanged();
                    }
                });

                builder.setNegativeButton(R.string.dialog_delete_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });


                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            }
        });
    }

    // onAddItem - Button handler
    public void onAddItem(View v) {
        // get the EditText view
        final EditText editText = (EditText) findViewById(R.id.input_insert_todo);
        // build the new TodoItem
        TodoItem item = new TodoItem(editText.getText().toString());
        // add to the ArrayList and notify the adapter
        todoItems.add(0, item);
        listViewAdapter.notifyDataSetChanged();
        // add to the DB and set the item idx
        long idx = dbAdapter.insert(item);
        item.setID(idx);
        // clear text field
        editText.setText("");
        // hide the softkey
        InputMethodManager imm = (InputMethodManager) getSystemService(
                MainActivity.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
        // EditText give up focus
        editText.clearFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
        // open the DB and update the listview
        dbAdapter.open();
        fillData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause()");
        // onPause should close the database so that it frees up resources
        // for other Activities. This is also called when the Activity is
        // destroyed or finished so there’s no need to close the database
        // “onFinish” or “onDestroy”.
        dbAdapter.close();
    }


    private void fillData() {
        Log.v(TAG, "Retriving all data to fill listview.");

        Cursor cursor = dbAdapter.getAllEntries();
        this.todoItems.clear();
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.TodoItems._ID));
            String task = cursor.getString(cursor
                    .getColumnIndex(DBContract.TodoItems.COLUMN_NAME_TASK));
            long created = cursor.getLong(cursor
                    .getColumnIndex(DBContract.TodoItems.COLUMN_NAME_CREATION_DATE));
            this.todoItems.add(0, new TodoItem(id, task, created));
            cursor.moveToNext();
        }
        cursor.close();
        listViewAdapter.notifyDataSetChanged();

        if (todoItems.isEmpty()) {
            String msg = this.getResources().getString(R.string.todolist_is_empty);
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
    }


}
