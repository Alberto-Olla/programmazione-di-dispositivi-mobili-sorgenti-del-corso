package pdm.a06.todolist;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/*
 * Database Open Helper
 */
class DBOpenHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = "DBOpenHelper";

    private static DBOpenHelper sInstance;  //singleton instance

    public static synchronized DBOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null)
            sInstance = new DBOpenHelper(context.getApplicationContext());

        return sInstance;
    }

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     */
    private DBOpenHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }

    //SQL-statement per la creazione della tabella del satabase.
    private static final String SQL_CREATE_TABLE_TODOITEMS = "create table " //
            + DBContract.TodoItems.TABLE_NAME + " (" //
            + DBContract.TodoItems._ID + " integer primary key autoincrement, " //
            + DBContract.TodoItems.COLUMN_NAME_TASK + " text not null, " //
            + DBContract.TodoItems.COLUMN_NAME_CREATION_DATE + " long" //
            + ");";

    @Override
    public void onCreate(SQLiteDatabase db) throws SQLException {
        Log.v(LOG_TAG, "Creating database: " + DBContract.DATABASE_NAME);
        try {
            db.execSQL(SQL_CREATE_TABLE_TODOITEMS);
        } catch (SQLException e) {
            Log.e(LOG_TAG, e.getMessage());
            throw e;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
