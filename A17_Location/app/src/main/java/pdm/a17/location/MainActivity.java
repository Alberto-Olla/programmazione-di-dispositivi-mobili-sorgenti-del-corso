package pdm.a17.location;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private final String TAG = this.getClass().getCanonicalName();
    private LocationListener myLocationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // uspdate location info in the GUI
        updateLocationInfo(null);

        // define the location lisetener
        myLocationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                switch (status) {
                    case LocationProvider.OUT_OF_SERVICE:
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        Toast.makeText(getApplicationContext(), "Location provider is not available.",
                                Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onProviderEnabled(String provider) {
                Toast.makeText(getApplicationContext(), "Location service <" + provider + "> enabled.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String provider) {
                Toast.makeText(getApplicationContext(), "Location service <" + provider + "> disabled.",
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLocationChanged(Location location) {
                updateLocationInfo(location);
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Using location manager. Checking permissions.");
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i(TAG, "Location permissions has NOT been granted. Requesting permissions.");
            requestLocationPermissions();
        } else {
            // Contact permissions have been granted. Show the contacts fragment.
            Log.i(TAG,
                    "Contact permissions have already been granted. Displaying contact details.");
            locationPermissionGranted = true;
        }
        if (locationPermissionGranted) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            updateLocationInfo(locationManager.getLastKnownLocation(provider));
            locationManager.requestLocationUpdates(provider, t, distance, myLocationListener);
        }
    }

    private boolean locationPermissionGranted = false;

    // Id to identify a location permission request.
    private static final int REQUEST_PERMISSION_LOCATION = 1;


    // Location permissions required
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    /**
     * Requests the Location permissions.
     * If the permission has been denied previously, an AlertDialog will prompt the user to grant
     * the permission, otherwise it is requested directly.
     */
    private void requestLocationPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i(TAG,
                    "Displaying location permission rationale to provide additional " +
                            "context.");

            // Build AlertDialog
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.permission_locations_rationale);
            builder.setPositiveButton("Require permission",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    PERMISSIONS_LOCATION,
                                    REQUEST_PERMISSION_LOCATION);
                        }
                    });

            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION,
                    REQUEST_PERMISSION_LOCATION);
        }
    }


    //Callback received when a permissions request has been completed
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            Log.i(TAG, "Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (PermissionUtil.verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
                Toast.makeText(getApplicationContext(), R.string
                                .permision_available_location,
                        Toast.LENGTH_SHORT).show();
                locationPermissionGranted = true;
            } else {
                Log.i(TAG, "Contacts permissions were NOT granted.");
                Toast.makeText(getApplicationContext(),
                        R.string.permissions_not_granted,
                        Toast.LENGTH_SHORT).show();
                locationPermissionGranted = false;
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    // upadate location info in the UI
    private void updateLocationInfo(Location location) {
        String latitude, longitude, altitude, accurancy;
        if (location == null)
            latitude = longitude = altitude = accurancy = getString(R.string.str_unknown);
        else {
            latitude = String.valueOf(location.getLatitude());
            longitude = String.valueOf(location.getLongitude());
            altitude = String.valueOf(location.getAltitude());
            accurancy = String.valueOf(location.getAccuracy());
        }
        ((TextView) findViewById(R.id.latitude)).setText("Latitude: " + latitude);
        ((TextView) findViewById(R.id.longitude)).setText("Longitude: " + longitude);
        ((TextView) findViewById(R.id.altitude)).setText("Altitude=" + altitude);
        ((TextView) findViewById(R.id.accurancy)).setText("Accurancy=" + accurancy + "m");
    }



    private String provider = LocationManager.GPS_PROVIDER;
    private int t = 5000; // milliseconds
    private int distance = 5; // meters


    @Override
    protected void onStop() {
        Log.i(TAG, "Using location manager. Checking permissions.");
        // Verify that all required contact permissions have been granted.
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i(TAG, "Location permissions has NOT been granted. Requesting permissions.");
            requestLocationPermissions();
        } else {
            // Contact permissions have been granted. Show the contacts fragment.
            Log.i(TAG,
                    "Contact permissions have already been granted. Displaying contact details.");
            locationPermissionGranted = true;
        }
        if (locationPermissionGranted) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            locationManager.removeUpdates(myLocationListener);
        }
        super.onStop();
    }

}
