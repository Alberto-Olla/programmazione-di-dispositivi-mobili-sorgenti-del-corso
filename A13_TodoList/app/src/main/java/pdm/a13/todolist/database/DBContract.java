package pdm.a13.todolist.database;

import android.provider.BaseColumns;

/* DB CONTRACT */
public class DBContract {
	
	// DATABASE 
	public static final String DATABASE_NAME = "todolist.db";
	public static final int DATABASE_VERSION = 3;


	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	private DBContract() {
	}

	/* Inner class that defines the table contents */
	public static abstract class TodoItems implements BaseColumns {

		public static final String TABLE_NAME = "todoItems";
		public static final String COLUMN_NAME_TASK = "task";
		public static final String COLUMN_NAME_IS_DUEDATE_REQUIRED = "duedate_required";
		public static final String COLUMN_NAME_CREATION_DATE = "creation_date";
		public static final String COLUMN_NAME_DUE_DATE = "due_date";
        public static final String COLUMN_NAME_URI_IMAGE = "uri_image";
	}
}
