package pdm.a04.buttonexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

	private int counter = 0;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate()");

        setContentView(R.layout.activity_main);
	}

    // BUTTONS onClick implementation
	public void onClickPlus(View v) {
		counter++;
        // we get the view references
        TextView output = (TextView) findViewById(R.id.output);
		output.setText(String.format(getResources().getString(R.string.counter),counter));
	}

	public void onClickMinus(View v) {
		counter--;
        TextView output = (TextView) findViewById(R.id.output);
        output.setText(String.format(getResources().getString(R.string.counter),counter));
	}

	public void onClickReset(View v) {
		counter = 0;
        TextView output = (TextView) findViewById(R.id.output);
        output.setText(String.format(getResources().getString(R.string.counter),counter));
	}



    // IMPLEMENTATION OF MAIN LIFECYCLE METHODS

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }
}